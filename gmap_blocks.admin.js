
$(function() {
  $('input[@name=disableDefaultUI]').click(function() {
    if ($(this).attr('checked')) {
      if ($('#control_set').hasClass('collapsed')) {
        Drupal.toggleFieldset($('#control_set'));
      };
    }
    else {
      if (!$('#control_set').hasClass('collapsed')) {
        Drupal.toggleFieldset($('#control_set'));
      };
    };
  });
});